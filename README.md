# Ruby

A collection of pipeline jobs that can be use with Ruby code.

| Component | Description |
| --------- | ----------- |
| `ruby` | Includes all jobs listed below |
| `ruby/test` | Run tests using RSpec testing framework |
| `ruby/lint` | Run Rubocop, the official Ruby linter |

## Inputs

| Input | Required | Default | Description |
| ----- | --- | ------- | ----------- |
| `image` | No | `ruby:latest` | The Docker image to use, depending on the Ruby version supported by the project. |

### All jobs

Includes all Ruby pipeline jobs defined in this project.

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/ruby@1.3
    with:
      image: ruby:latest
```

### Test job

Run tests using RSpec testing framework.

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/ruby/test@1.3
```

### Lint job

Run Rubocop, the official Ruby linter.

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/ruby/lint@1.3
```
